/*
************************************************************************************************************************
*                                                         eGON
*                                         the Embedded GO-ON Bootloader System
*
*                             Copyright(C), 2006-2008, SoftWinners Microelectronic Co., Ltd.
*											       All Rights Reserved
*
* File Name : Boot1_head.c
*
* Author : Gary.Wang
*
* Version : 1.1.0
*
* Date : 2007.11.06
*
* Description : This file defines the file head part of Boot1, which contains some important
*             infomations such as magic, platform infomation and so on, and MUST be allocted in the
*             head of Boot1.
*
* Others : None at present.
*
*
* History :
*
*  <Author>        <time>       <version>      <description>
*
* Gary.Wang       2007.11.06      1.1.0        build the file
*
************************************************************************************************************************
*/
#include "boot1_v2.h"

#define BOOT1_PRVT_HEAD_VERSION         "1230"    // X.X.XX
#define BOOT1_FILE_HEAD_VERSION         "1230"    // X.X.XX
#define BOOT1_VERSION                   "1230"    // X.X.XX

const boot1_file_head_t  BT1_head __attribute__ ((section (".header"))) = {
                                      {
          /* jump_instruction */          ( 0xEA000000 | ( ( ( sizeof( boot1_file_head_t ) + sizeof( int ) - 1 ) / sizeof( int ) - 2 ) & 0x00FFFFFF ) ),
                                          BOOT1_MAGIC,
                                          STAMP_VALUE,
                                          0,
                                          0,
                                          0x4000,
                                          BOOT1_FILE_HEAD_VERSION,
                                          BOOT1_VERSION,
                                          EGON_VERSION,
                                          {
                                            '2','.','0','.','0',0,0,0
                                          },
                                      },
                                      {
                                          sizeof( boot1_file_head_t ),
                                          BOOT1_PRVT_HEAD_VERSION,
                                          0,
                                          { 0 },
                                          { 0 },
                                          { 0 },
                                          { 0 },
                                          0,
                                          { 0 },
                                          0,
                                          0,
                                          0,
                                          0,
                                          0,
                                          { 0 },
                                          { 0 }
                                      }
                                  };





