#include "arm_a8.h"

.global _start
.arm


.section .init
_start:
        mrs r0, cpsr
        bic r0, r0, #ARMV7_MODE_MASK
        orr r0, r0, #ARMV7_SVC_MODE
        orr r0, r0, #( ARMV7_IRQ_MASK | ARMV7_FIQ_MASK )
        bic r0, r0, #ARMV7_CC_E_BIT
        msr cpsr_c, r0

        mrc p15, 0, r0, c1, c0
        bic r0, r0, #( ARMV7_C1_M_BIT | ARMV7_C1_C_BIT )
        bic r0, r0, #( ARMV7_C1_I_BIT | ARMV7_C1_Z_BIT )
        bic r0, r0, #( ARMV7_C1_A_BIT)
        mcr p15, 0, r0, c1, c0

        ldr sp, =_estack

        ldr	r2, =_sbss
        b	LoopFillZerobss

FillZerobss:
        movs	r3, #0
        str	r3, [r2], #4

LoopFillZerobss:
        ldr	r3, = _ebss
        cmp	r2, r3
        bcc	FillZerobss

        bl  main

        b .

.end
