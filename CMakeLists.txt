set (CMAKE_TOOLCHAIN_FILE "${CMAKE_SOURCE_DIR}/cmake/stm32Toolchain.cmake")
include( "${CMAKE_SOURCE_DIR}/cmake/functions.cmake" )

CMAKE_MINIMUM_REQUIRED(VERSION 3.2.0)

get_filename_component(ProjectId ${CMAKE_CURRENT_SOURCE_DIR} NAME)
string(REPLACE " " "_" ProjectId ${ProjectId})
PROJECT(${ProjectId})


ENABLE_LANGUAGE(C ASM)

set(BINARY_NAME "${CMAKE_PROJECT_NAME}")
set(BINARY_PATH "${CMAKE_BINARY_DIR}/${BINARY_NAME}")

set(CMAKE_C_FLAGS "-nostartfiles -nodefaultlibs -nostdlib")

# Add all subdirectories ${PROJECT_SOURCE_DIR}/src as sources:
SUBDIRLIST(SUBDIRS ${PROJECT_SOURCE_DIR}/src)
SUBDIRLIST(SUBDIRS ${PROJECT_SOURCE_DIR}/include)

foreach(subdir ${SUBDIRS})
   add_subdirectory(${subdir})
endforeach()

set_property(SOURCE ${SOURCES} PROPERTY LANGUAGE C)

if (CMAKE_BUILD_TYPE STREQUAL "Debug")
    SET(CMAKE_ASM_FLAGS_DEBUG " ${CMAKE_ASM_FLAGS_DEBUG} -I ${CMAKE_SOURCE_DIR}/include")
endif()

set(BINARY_PATH "${CMAKE_BINARY_DIR}/${BINARY_NAME}")

if (CMAKE_BUILD_TYPE STREQUAL "Release")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -s")
endif()

if (CMAKE_BUILD_TYPE STREQUAL "MinSizeRel")
    set(CMAKE_ASM_FLAGS_MINSIZEREL " ")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -s")
endif()

if (CMAKE_BUILD_TYPE STREQUAL "RelWithDebInfo")
    set(CMAKE_ASM_FLAGS_RELWITHDEBINFO " ")
endif()

include_directories(${CMAKE_SOURCE_DIR}/include)

set(LINKER_SCRIPT "${CMAKE_SOURCE_DIR}/linker/system.ld")
set(LINKER_MAP_FILE "${CMAKE_SOURCE_DIR}/linker/${BINARY_NAME}.map")
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -T ${LINKER_SCRIPT} -Xlinker -Map=${BINARY_NAME}.map")

add_executable(${BINARY_NAME} ${SOURCES} ${INCLUDES})

target_link_libraries(${BINARY_NAME} ${USER_LIBS} gcc c)

add_custom_command(TARGET ${BINARY_NAME} POST_BUILD
    COMMAND ${CMAKE_SIZE} ${BINARY_PATH})

add_custom_command(TARGET ${BINARY_NAME} POST_BUILD
    COMMAND ${CMAKE_OBJCOPY} -O binary ${BINARY_PATH} ${CMAKE_BINARY_DIR}/${BINARY_NAME}.bin -S -g )


#add_custom_command(TARGET ${BINARY_NAME} POST_BUILD
#    COMMAND ${CMAKE_OBJCOPY} -O elf-i386 ${BINARY_PATH} ${CMAKE_BINARY_DIR}/${BINARY_NAME}.elf )

add_custom_target(flash COMMAND echo "======== flash ========" COMMAND ${CMAKE_SOURCE_DIR}/openocd/flash.sh ${CMAKE_BINARY_DIR}/${BINARY_NAME} ${CMAKE_SOURCE_DIR}/)

add_custom_target(debug COMMAND qemu-system-arm -machine cubieboard -cpu cortex-a8 -s -S -m 1G -kernel ${CMAKE_BINARY_DIR}/${BINARY_NAME})
add_custom_target(run COMMAND qemu-system-arm -machine cubieboard -cpu cortex-a8 -serial stdio -append "console=tty1" -m 1G -kernel ${CMAKE_BINARY_DIR}/${BINARY_NAME})


